/*
 * purple-mumble -- Mumble protocol plugin for libpurple
 * Copyright (C) 2018  Petteri Pitkänen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <purple.h>
#include <glib/gi18n.h>
#include "mumble-protocol.h"

#if PURPLE_VERSION_CHECK(3, 0, 0)
static PurplePluginInfo *plugin_query(GError **);
static gboolean plugin_load(PurplePlugin *, GError **);
static gboolean plugin_unload(PurplePlugin *, GError **);

PURPLE_PLUGIN_INIT(mumble, plugin_query, plugin_load, plugin_unload);

static PurpleProtocol *purpleProtocol;

static PurplePluginInfo *plugin_query(GError **error) {
  return purple_plugin_info_new(
    "id",          "prpl-mumble",
    "abi-version", PURPLE_ABI_VERSION,
    "name",        "Mumble protocol",
    "version",     "0.0.1",
    "category",    N_("Protocol"),
    "summary",     N_("Mumble protocol plugin"),
    "description", N_("Mumble protocol plugin that supports only text."),
    "license-id",  "GPL",
    "flags",       PURPLE_PLUGIN_INFO_FLAGS_AUTO_LOAD,
    NULL
  );
}

static gboolean plugin_load(PurplePlugin *plugin, GError **error) {
  mumble_protocol_register_type(plugin);
  
  purpleProtocol = purple_protocols_add(MUMBLE_TYPE_PROTOCOL, error);
  if (!purpleProtocol) {
    return FALSE;
  }
  
  return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin, GError **error) {
  if (!purple_protocols_remove(purpleProtocol, error)) {
    return FALSE;
  }
  
  return TRUE;
}

#else
	// purple2

static gboolean
plugin_load(PurplePlugin *plugin)
{
	return TRUE;
}

static gboolean
plugin_unload(PurplePlugin *plugin)
{
	return TRUE;
}

static PurplePluginInfo info =
{
	PURPLE_PLUGIN_MAGIC,
	PURPLE_MAJOR_VERSION,
	PURPLE_MINOR_VERSION,
	PURPLE_PLUGIN_PROTOCOL,                                /**< type           */
	NULL,                                                  /**< ui_requirement */
	0,                                                     /**< flags          */
	NULL,                                                  /**< dependencies   */
	PURPLE_PRIORITY_DEFAULT,                               /**< priority       */
                                                       
	"prpl-mumble",                                         /**< id             */
	N_("Mumble protocol"),                                 /**< name           */
	"0.0.1",                                               /**< version        */
	                                                   
	N_("Mumble protocol plugins."),                        /**< summary        */
	                                                     
	N_("Mumble protocol plugin that supports only text."), /**< description    */
	"",                                                    /**< author         */
	"",  /**< homepage       */

	plugin_load,                                           /**< load           */
	plugin_unload,                                         /**< unload         */
	NULL,                                                  /**< destroy        */
                                                      
	NULL,                                                  /**< ui_info        */
	NULL,                                                  /**< extra_info     */
	NULL,                                                  /**< prefs_info     */
	NULL,                                                  /**< actions        */

	/* padding */
	NULL,
	NULL,
	NULL,
	NULL
};
	
PURPLE_INIT_PLUGIN(mumble, mumble_protocol_init, info);


#endif